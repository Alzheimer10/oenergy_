<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {
 
	Route::get('/','oEnergy\oEnergyController@home')->name('home');
	Route::get('/projects','oEnergy\oEnergyController@projects')->name('projects');
	Route::get('/services','oEnergy\oEnergyController@services')->name('services');
	Route::get('/services/{service}','oEnergy\oEnergyController@service')->name('service');
	Route::get('/contact','oEnergy\oEnergyController@contact')->name('contact');
	Route::get('/about','oEnergy\oEnergyController@about')->name('about');
	Route::get('/blog','oEnergy\oEnergyController@blog')->name('blog');
	Route::get('/fap','oEnergy\oEnergyController@fap')->name('fap');
	Route::get('/history','oEnergy\oEnergyController@history')->name('history');
	Route::get('/values','oEnergy\oEnergyController@values')->name('values');
	Route::get('/competition','oEnergy\oEnergyController@competition')->name('competition');
	Route::get('/team/{boss?}','oEnergy\oEnergyController@team')->name('team');
	Route::get('/electrical_systems','oEnergy\oEnergyController@electrical_systems')->name('electrical_systems');

	Route::get('lang/{lang}', function ($lang) {
	    session(['lang' => $lang]);
	    return \Redirect::back();
	})->where([
	    'lang' => 'en|es'
	]);
});