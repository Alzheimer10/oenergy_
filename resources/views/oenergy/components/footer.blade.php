<!-- footer -->
<footer class="container-fluid">
  <div class="footer__top">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-4 sr_bottom">
          <ul class="social">
            <li class="social__1">  <a href="https://www.linkedin.com/company/oenergy" target="_blank"> <i class="fa fa-linkedin"></i>       </a></li>
            <li class="social__2">  <a href="https://www.facebook.com/oenergy/" target="_blank">        <i class="fa fa-facebook"></i>       </a></li>
            <li class="social__3">  <a href="https://twitter.com/oEnergyspa" target="_blank">           <i class="fa fa-twitter"></i>        </a></li>
            <li class="social__4">  <a href="https://plus.google.com/+oenergycl" target="_blank">       <i class="fa fa-google-plus"></i>    </a></li>
            <li class="social__5">  <a href="#" target="_blank">                                        <i class="fa fa-instagram"></i>      </a></li>
          </ul>
        </div>

        <div class="col-xs-12 col-sm-7 col-md-8 sr_bottom">
          <ul class="footer_contact">
            <li><a href="https://www.google.cl/maps/place/Av.+Nueva+Providencia+1881,+Providencia,+Regi%C3%B3n+Metropolitana/@-33.4257525,-70.6139826,20.41z/data=!4m5!3m4!1s0x9662cf64434a2451:0xea3d644731589924!8m2!3d-33.4258238!4d-70.6138651"><i class="fa fa-map-marker fa__rect"></i> Matriz: Avenida Nueva Providencia 1881 Oficina 318 Providencia, Región Metropolitana, Chile. </a></li>
            <li><a href="https://www.google.com/maps/place/Santiago,+Regi%C3%B3n+Metropolitana,+Chile/@-33.4727092,-70.7699154,11z/data=!3m1!4b1!4m5!3m4!1s0x9662c5410425af2f:0x8475d53c400f0931!8m2!3d-33.4488897!4d-70.6692655?hl=es-ES"><i class="fa fa-map-marker fa__rect"></i> Sucursal: Sargento Aldea N° 942, local N° 5 Chillán, Chile. </a></li>
            <li class="inline-block"><a href="tel:+56229695077"><i class="fa fa-phone fa__rect"></i> +56 2 2969 5077 </a></li>
            <li class="inline-block inline-block__m"><a href="mailto:info@oenergy.cl"><i class="fa fa-envelope-open fa__rect"></i> info@oenergy.cl </a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="footer__bottom text-center container sr_bottom">
    <div class="col-xs-12">
      <span>Copyright © {{date('Y')}} oEnergy</span>
      <ul class="list-inline">
        <li class="list-inline-item"><a href="{{ route('services') }}">@lang('app.servicios')</a>/</li>
        <li class="list-inline-item"><a href="{{ route('projects') }}">@lang('app.proyectos')</a>/</li>
        <li class="list-inline-item"><a href="{{ route('contact') }}">@lang('app.contáctenos')</a>/</li>
        <li class="list-inline-item"><a href="#">@lang('app.política de privacidad')</a>/</li>
        <li class="list-inline-item"><a href="{{ route('fap') }}">@lang('app.preguntas frecuentes')</a></li>
      </ul>
    </div>
  </div>
</footer>
<!-- footer-end -->