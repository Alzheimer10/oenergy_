<nav class="navbar navbar-default">
  <div class="container container__navbar">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-master" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand nav-visibility-xs pt-0" href="{{ route('home') }}">
        <img class="d-inline-block align-top" height="49.2px" alt="logo_home" src="{{ asset('img/logo/logo_x.png') }}">
      </a>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse-master">
      <ul class="nav navbar-nav nav__left nav-hidden-xs">
        <li>
          <a class="nav-link p-5" href="{{ route('home') }}"><img class="d-inline-block align-top" height="49.2px" alt="logo_home" src="{{ asset('img/logo/logo_x.png') }}"></a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-collapse_xs nav__right" data-hover="dropdown"  data-animations="bounceInLeft bounceInRight fadeInUp fadeInRight">
        <!-- home -->
        <li class="btn-inicio @if(Request::is('/')) active @endif"><a class="nav-link ttc" href="{{ route('home') }}">@lang('app.inicio')</a></li>
        <!-- projects -->
        <li class="@if(Request::is('projects')) active @endif"><a class="nav-link ttc" href="{{ route('projects') }}">@lang('app.proyectos')</a></li>
        <!-- services -->
        <li class="@if(Request::is('services')) active @endif"><a class="nav-link ttc" href="{{ route('services') }}">@lang('app.servicios')</a></li>
        <!-- about us -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle ttc" type="button" data-toggle="dropdown" data-hover="dropdown" >
            {{trans('app.quienes somos')}}<span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li class="@if(Request::is('about')) active @endif"><a class="dropdown-item ttc" href="{{ route('about') }}">@lang('app.empresa')</a></li>
            <li class="@if(Request::is('history')) active @endif"><a class="dropdown-item ttc" href="{{ route('history') }}">@lang('app.historia')</a></li>
            <li class="@if(Request::is('values')) active @endif"><a class="dropdown-item ttc" href="{{ route('values') }}">@lang('app.valores')</a></li>
            <li class="@if(Request::is('competition')) active @endif"><a class="dropdown-item ttc" href="{{ route('competition') }}">@lang('app.nuestra competencias')</a></li>
            <li class="@if(Request::is('team')) active @endif"><a class="dropdown-item ttc" href="{{ route('team') }}">@lang('app.nuestro equipo')</a></li>
          </ul>
        </li>
        <!-- clientes -->
        {{-- <li><a class="nav-link" href="http://www.oenergy.cl/clientes/login" target="_blank">@lang('app.clientes')</a></li> --}}
        {{-- <li class="@if(Request::is('blog')) active @endif"><a class="nav-link" href="{{ route('blog') }}">@lang('app.blog')</a></li> --}}
        <!-- LANG -->
        @if( session('lang')=='es' || session('lang')=='' )
          <li><a href="{{ url('lang', ['en']) }}" class="dropdown-item" ><img src="{{ asset('img/lang/eng.png') }}" alt="ico_eng"> @lang('app.ingles')</a></li>
        @else
          <li><a href="{{ url('lang', ['es']) }}" class="dropdown-item" ><img src="{{ asset('img/lang/esp.png') }}" alt="ico_esp"> @lang('app.español')</a></li>
        @endif
        <!--         <li class="dropdown dropdown-lang">
          @if( session('lang')=='es' || session('lang')=='' )
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Es<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="{{ url('lang', ['en']) }}" class="dropdown-item" ><img src="{{ asset('img/lang/eng.png') }}" alt="ico_eng"> En</a></li>
            </ul>
          @else
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">En<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="{{ url('lang', ['es']) }}" class="dropdown-item" ><img src="{{ asset('img/lang/esp.png') }}" alt="ico_esp"> Es</a></li>
            </ul>
          @endif
        </li> -->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>