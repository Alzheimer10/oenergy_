@extends('oenergy.layout.master')
@section('title', 'contact')
@section('styles')
<!-- scripts for page:contact -->
@stop

@section('content')
<!-- content for page:contact -->
<div class="container container__main">
<!--     <div class="container-fluid">
	<div class="row">
    	<h1  class="border__bottom"></h1>
	</div>
</div> -->
    <div class="container-fluid">
        <div class="row">
        	<!-- FORM CONTACT-->
        	<div class="col-xs-12 col-sm-6">
        		<div class="row">
	        		<form>
						<div class="col-xs-12">
		        			<h4 class="border__bottom ttu">@lang('app.texto_contacto_1')</h4>
		        			<p>@lang('app.texto_contacto_2')</p>
						</div>

				        <div class="col-xs-12 col-sm-6">
				        	<div class="input-effect">
		        				<input type="text" name="name" id="inputName" class="effect-19 focus-border" required>
					            <label for="inputName " class="ttc">@lang('app.nombre')</label>
					            <span class="focus-border">
					            	<i></i>
					            </span>
				        	</div>
				        </div>

				        <div class="col-xs-12 col-sm-6">
					        <div class="input-effect">
		        				<input type="text" name="lastname" id="lastname" class="effect-20" required>
					            <label for="lastname " class="ttc">@lang('app.apellido')</label>
					            <span class="focus-border">
					            	<i></i>
					            </span>
				        	</div>
				        </div>

				        <div class="col-xs-12">
					        <div class="input-effect">
	        					<input type="text" name="email" id="email" class="effect-19"  required>
				            	<label for="email " class="ttc">@lang('app.correo')</label>
				            	<span class="focus-border">
				            		<i></i>
				            	</span>
				        	</div>
				        </div>

				        <div class="col-xs-12">
					        <div class="input-effect">
		        				<textarea type="text" id="messageLabel" for="message" class="effect-20" style="width: 100%;" rows="4"></textarea>  
					            <label for="message " class="ttc">@lang('app.mensaje')</label>
					            <span class="focus-border">
					            	<i></i>
					            </span>
				        	</div>
				        </div>

	        			<div class="col-xs-12">
							<div class="text-center margin-top-25">
							    <button type="submit" class="btn btn-outline-primary btn-lg ttc" style="margin-top: 20px">@lang('app.enviar')</button>
							</div>
	        			</div>
	        		</form>
        		</div>
        	</div>
        	<!-- FORM CONTACT-END -->
        	
        	<!-- ADDRESSES-->
        	<div class="col-xs-12 col-sm-6">
        		<div class="container">
					<h4 class="border__bottom ttu">@lang('app.direcciones')</h4>
					<ul class="footer_contact footer_contact-inv">
				        <li><a href="https://www.google.cl/maps/place/Av.+Nueva+Providencia+1881,+Providencia,+Regi%C3%B3n+Metropolitana/@-33.4257525,-70.6139826,20.41z/data=!4m5!3m4!1s0x9662cf64434a2451:0xea3d644731589924!8m2!3d-33.4258238!4d-70.6138651" target="_blank"><i class="fa fa-map-marker fa__rect"></i> Matriz: Avenida Nueva Providencia 1881 Oficina 318 Providencia, Región Metropolitana, Chile. </a></li>
				        <li><a href="https://www.google.com/maps/place/Santiago,+Regi%C3%B3n+Metropolitana,+Chile/@-33.4727092,-70.7699154,11z/data=!3m1!4b1!4m5!3m4!1s0x9662c5410425af2f:0x8475d53c400f0931!8m2!3d-33.4488897!4d-70.6692655?hl=es-ES target="_blank"><i class="fa fa-map-marker fa__rect"></i> Sucursal: Sargento Aldea N° 942, local N° 5 Chillán, Chile. </a></li>
				        <li class="inline-block"><a href="tel:+56229695077"><i class="fa fa-phone fa__rect"></i> +56 2 2969 5077</a></li>
				        <li class="inline-block inline-block__m"><a href="mailto:info@oenergy.cl"><i class="fa fa-envelope-open fa__rect"></i> info@oenergy.cl </a></li>
			        </ul>
			        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d12203781.161032058!2d-42.832111!3d-41.689463!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2sus!4v1515454877456" width="100%" frameborder="0" style="border:0;min-height: 350px; width: 100%" allowfullscreen></iframe>
				</div>
        	</div>
        	<!-- ADDRESSES-END-->
        </div>
    </div>
</div>
<!-- content-end for page:contact -->
@stop

@section('scripts')
<!-- scripts for page:contact -->
<script>
	// JavaScript for label effects only
	$(document).ready(function(){
		$(".input-effect [class*='effect-']").val("");
		$(".input-effect [class*='effect-']").focusout(function(){
			if($(this).val() != "")
				$(this).addClass("has-content");
			else
				$(this).removeClass("has-content");
		})
	});

</script>
@stop