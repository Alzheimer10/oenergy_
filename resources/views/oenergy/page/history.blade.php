@extends('oenergy.layout.master')
@section('title', 'history')
@section('styles')
<!-- scripts for page:history -->
@stop

@section('content')
<!-- content for page:history -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom">HISTORIA DE OENERGY</h1>
    </div>
    <div class="container-fluid">
		<div class="row">
			<p>oEnergy SpA se establece el año 2013 tras un extensivo estudio de mercado que dio a luz la tardía incorporación e implementación de las energías renovables no convencionales (ERNC) a pequeña y mediana escala en Chile. El estudio indicó que los principales motivos por los cuales las ERNC no se han masificado en Chile son la:</p>
			<ul>
				<li>Falta de difusión y capacitación a las pequeñas y medianas empresas (PYMES) sobre los modelos de negocio de proyectos de eficiencia energética.</li>
				<li>Falta de una política de energía nacional dirigida a las PYMES que incentive y facilite las inversiones en los distintos sectores productivos del país.</li>
				<li>Escasez de empresas desarrolladoras de proyectos llave-en-mano que puedan ofrecer soluciones individualizadas y que garanticen los ahorros proyectados.</li>
			</ul>
			<p>El estudio además indicó la gran necesidad de las PYMES, municipalidades, clientes aislados de la red y clientes regulados con alto consumo energético en disminuir considerablemente sus gastos operacionales.</p>
			<p>Aunque cada uno de nuestros clientes tiene un perfil distinto, todos cumplen con una o ambas de las siguientes características que le permiten invertir en proyectos de eficiencia energética y generar ahorros:</p>
			<ul>
				<li>Disponen de un recurso natural cercano al emplazamiento (río, mar, sol y/o viento). Mientras mayor sea el potencial del recurso natural disponible, mayor rentabilidad tendrá la inversión del cliente.</li>
				<li>Actualmente esten pagando sumas muy altas por concepto de energía a la distribuidora eléctrica local o para comprar y transportar combustible en caso de clientes aislados. Mientras mayor sea el gasto de energía actual, mayor rentabilidad tendrá la inversión del cliente.</li>
			</ul>
			<p>En cualquiera de estos casos, nuestros clientes pueden alcanzar un nivel de autonomía energética y crear inmunidad a las futuras alzas de los precios de la energía. Por favor contáctese con nosotros para conocer más sobre como usted puede recibir una evaluación gratuita de Factibilidad técnico-económica y acceder a financiamiento y/o subsidios que le faciliten tomar la decisión de invertir en un proyecto de co-generación renovable.</p>
		</div>
	</div>
</div>
<!-- content-end for page:history -->
@stop

@section('scripts')
<!-- scripts for page:history -->
@stop