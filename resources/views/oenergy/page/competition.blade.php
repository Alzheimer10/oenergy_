@extends('oenergy.layout.master')
@section('title', 'competition')
<!-- scripts for page:competition -->
@section('styles')
@stop

@section('content')
<!-- content for page:competition -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom">NUESTRA COMPETENCIAS</h1>
    </div>
    <div class="container-fluid">
		<div class="row">
        	<p>Nuestros ingenieros multidisciplinarios y con más de 25 años de experiencia, son capaces de llevar a cabo las diferentes etapas de un proyecto en el marco de:</p>
			<p><b>Factibilidad Técnico-Económico-Legal y Desarrollo de Proyectos de Generación ERNC</b></p>
			<ul>
				<li>Estudios ambientales (pertinencias, DIA y EIA).</li>
				<li>Estudios eléctricos sistémicos de impacto a la red (troncal, subtransmisión y distribución).</li>
				<li>Estudios de dimensionamiento de recurso natural (radiación, viento, corrientes marinas, etc.).</li>
				<li>Pertinencia y concesiones mineras , usufructos, compraventa y arrendamiento de inmuebles.</li>
			</ul>
			<p><b>Diseño e Ingeniería</b></p>
			<ul>
				<li>Diseños usando distintas tecnologías basado en los estudios de factibilidad listados.</li>
				<li>Ingeniería de concepto, básica, detalle y construcción.</li>
			</ul>
			<p><b>Financiamiento</b></p>
			<ul>
				<li>Ayuda en la postulación de proyectos a fondos concursales de subsidio (CORFO, CER, CNR, AChEE, FNDR, FIC, FIA, Mercado Público, etc.).</li>
			</ul>
			<p><b>Due Diligence</b></p>
			<ul>
				<li>Auditoría y asesoramiento técnico en la compra de proyectos de generación ERNC.</li>
			</ul>
			<p><b>Ingeniería, Adquisición y Construcción (EPC)</b></p>
			
			<ul>
				<li>Gestión de permisos sectoriales ambientales, salud y construcción.</li>
				<li>Gerenciamiento de construcción.</li>
				<li>Compra e importación directa de equipos e insumos.</li>
				<li>Instalación de faenas y puesta en marcha.</li>
			</ul>
			<p><b>Servicio de Operación y Mantención</b></p>
		</div>
	</div>
</div>
<!-- content-end for page:competition -->
@stop

@section('scripts')
<!-- scripts for page:competition -->
@stop