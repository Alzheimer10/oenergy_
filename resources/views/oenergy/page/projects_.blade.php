@extends('oenergy.layout.master')
@section('title', 'projects')
<!-- scripts for page:projects -->
@section('styles')
  <link rel="stylesheet" href="{{ asset('modules/jquery.magnific-popup/magnific-popup.css') }}">
@stop

@section('content')
<!-- content for page:projects -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom">NUESTROS PROYECTOS</h1>
        <p>Somos una empresa formada por un equipo multidisciplinario que se enfoca en proyectos llave-en-mano y de tipo Energy Savings Company (ESCO) de generación eléctrica renovable con potencias entre 2,5KW y 3MW.</p>
    </div>
    <div class="container-fluid">
      <div id="grid" data-columns="3">
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/01.jpeg') }}">
          			<img src="{{ asset('img/projects/01_thumbnail.jpg') }}"	class="shadow" alt="01_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/02.jpeg') }}">
          			<img src="{{ asset('img/projects/02_thumbnail.jpg') }}"	class="shadow" alt="02_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/03.jpeg') }}">
          			<img src="{{ asset('img/projects/03_thumbnail.jpg') }}"	class="shadow" alt="03_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/04.jpeg') }}">
          			<img src="{{ asset('img/projects/04_thumbnail.jpg') }}"	class="shadow" alt="04_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/05.jpeg') }}">
          			<img src="{{ asset('img/projects/05_thumbnail.jpg') }}"	class="shadow" alt="05_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/06.jpeg') }}">
          			<img src="{{ asset('img/projects/06_thumbnail.jpg') }}"	class="shadow" alt="06_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/07.jpeg') }}">
          			<img src="{{ asset('img/projects/07_thumbnail.jpg') }}"	class="shadow" alt="07_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/08.jpeg') }}">
          			<img src="{{ asset('img/projects/08_thumbnail.jpg') }}"	class="shadow" alt="08_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/09.jpeg') }}">
          			<img src="{{ asset('img/projects/09_thumbnail.jpg') }}"	class="shadow" alt="09_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/10.jpeg') }}">
          			<img src="{{ asset('img/projects/10_thumbnail.jpg') }}"	class="shadow" alt="10_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/11.jpeg') }}">
          			<img src="{{ asset('img/projects/11_thumbnail.jpg') }}"	class="shadow" alt="11_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/12.jpeg') }}">
          			<img src="{{ asset('img/projects/12_thumbnail.jpg') }}"	class="shadow" alt="12_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/13.jpeg') }}">
          			<img src="{{ asset('img/projects/13_thumbnail.jpg') }}"	class="shadow" alt="13_thumbnail.jpg"/>
          		</a>
        	</div>
          	<div class="image_column card__shadow">
          		<a href="{{ asset('img/projects/14.jpeg') }}">
          			<img src="{{ asset('img/projects/14_thumbnail.jpg') }}"	class="shadow" alt="14_thumbnail.jpg"/>
          		</a>
        	</div>
      </div>
    </div>
</div>
<!-- content-end for page:projects -->
@stop

@section('scripts')
<!-- scripts for page:projects -->
    <script type="text/javascript" src="{{ asset('modules/salvattore/salvattore.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('modules/jquery.magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script>
		$('.card__shadow a').magnificPopup({
			type: 'image',
			gallery: {
			  enabled: true
			}
		});
    </script>
@stop