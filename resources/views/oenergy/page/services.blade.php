@extends('oenergy.layout.master')
@section('title', 'services')
<!-- scripts for page:services -->
@section('styles')
@stop

@section('content')
<!-- content for page:services -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom">@lang('app.nuestros servicios')</h1>
    </div>

    <div class="container-fluid">
		<!--PEN CODE-->
		<div class="row">
			<div data-columns="3">
		    	<div class="image_column size-1of4">
					<div class="card card_1">
						<div class="card-stuff">
							<h4 class="card__title">@lang('app.estudios_sistemas_electricos')</h4>
							<p class="card__description">@lang('app.estudios_sistemas_electricos_description')</p>
							<div class="card__btn">
				  				<a href="{{ route('service', 'estudios_sistemas_electricos') }}" class="btn btn-outline-primary">@lang('app.saber_mas')</a>
							</div>
						</div>
					</div>"
					<div class="card card_4">
						<div class="card-stuff">
							<h4 class="card__title">@lang('app.servicios_terreno')</h4>
							<p class="card__description">@lang('app.servicios_terreno_description')</p>
							<div class="card__btn">
				  				<a href="{{ route('service', 'servicios_terreno') }}" class="btn btn-outline-primary">@lang('app.saber_mas')</a>
							</div>
						</div>
					</div>
		    	</div>
		    	<div class="image_column size-1of4">
					<div class="card  card_3">
						<div class="card-stuff">
							<h4 class="card__title">@lang('app.operacion_mantención')</h4>
							<p class="card__description">@lang('app.operacion_mantención_description')</p>
							<div class="card__btn">
				  				<a href="{{ route('service', 'operacion_mantención') }}" class="btn btn-outline-primary">@lang('app.saber_mas')</a>
							</div>
						</div>
					</div>
					<div class="card card_2">
						<div class="card-stuff">
							<h4 class="card__title">@lang('app.financiamiento_subsidios')</h4>
							<p class="card__description">@lang('app.financiamiento_subsidios_description')</p>
							<div class="card__btn">
				  				<a href="{{ route('service', 'financiamiento_subsidios') }}" class="btn btn-outline-primary">@lang('app.saber_mas')</a>
							</div>
						</div>
					</div>
		    	</div>
		    	<div class="image_column size-1of4">
					<div class="card card_5">
						<div class="card-stuff">
							<h4 class="card__title">@lang('app.factibilidad')</h4>
							<p class="card__description">@lang('app.factibilidad_description')</p>
							<div class="card__btn">
				  				<a href="{{ route('service', 'factibilidad') }}" class="btn btn-outline-primary">@lang('app.saber_mas')</a>
							</div>
						</div>
					</div>
					<div class="card card_6">
						<div class="card-stuff">
							<h4 class="card__title">@lang('app.construccion')</h4>
							<p class="card__description">@lang('app.factibilidad_description')</p>
							<div class="card__btn">
				  				<a href="{{ route('service', 'construccion') }}" class="btn btn-outline-primary">@lang('app.saber_mas')</a>
							</div>
						</div>
					</div>
		    	</div>
		    	<div class="image_column size-1of4">
					<div class="card card_8">
						<div class="card-stuff">
							<h4 class="card__title">@lang('app.due_diligence')</h4>
							<p class="card__description">@lang('app.due_diligence_description')</p>
							<div class="card__btn">
				  				<a href="{{ route('service', 'due_diligence') }}" class="btn btn-outline-primary">@lang('app.saber_mas')</a>
							</div>
						</div>
					</div>
					<div class="card card_7">
						<div class="card-stuff">
							<h4 class="card__title">@lang('app.diseño_desarrollo')</h4>
							<p class="card__description">@lang('app.diseño_desarrollo_description')</p>
							<div class="card__btn">
				  				<a href="{{ route('service', 'diseño_desarrollo') }}" class="btn btn-outline-primary">@lang('app.saber_mas')</a>
							</div>
						</div>
					</div>
		    	</div>
			</div>
		</div>
		<!--END PEN CODE-->
    </div>
</div>
<!-- content for page:services -->
@stop

@section('scripts')
<!-- scripts for page:services -->
@stop