@extends('oenergy.layout.master')
@section('title', 'fap')
@section('styles')
<!-- scripts for page:fap -->
@stop

@section('content')
<!-- content for page:fap -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom">PREGUNTAS FRECUENTES</h1>
        <p>Se ofrece aquí respuesta a una selección de las preguntas más frecuentes planteadas por los usuarios del servicio de consultas lingüísticas.</p>
    </div>
</div>
<!-- content-end for page:fap -->
@stop

@section('scripts')
<!-- scripts for page:fap -->
@stop