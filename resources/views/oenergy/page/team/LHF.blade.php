<div class="container container_boss">
	<a href="{{ route('team') }}" class="btn btn-outline-primary">atras</a>
	<div class="container_img">
		<img src="{{ asset('img/team/LHFT.png') }}" class="boss_img" />
	</div>
	<br>
	<h1 class="boss_name">LIBARDO HERNÁNDEZ FERNÁNDEZ</h1>
	<h3 class="boss_position">Gerente de Ingeniería y Construcción</h3>
		<ul class="boss_social">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-instagram"></i></a></li>
		</ul>
	<p>
		Libardo posee 30 años de experiencia profesional en docencia universitaria, investigación científica, mantenimiento industrial, asesoría en ingeniería eléctrica y de instrumentación para la gran minera y plantas de celulosa, gerencia de ingeniería de proyectos y gerencia de contratos de servicio, mantenimiento y construcción. Libardo se desempeñó en sus varias capacidades para proveer soluciones de eficiencia energética y generación renovable a compañías como Minera Los Pelambres, Minera Collahuasi, Minera Candelaria, Codelco División Andina, Codelco División El Teniente, Minera Mantos Blancos, Teck Minera Carmen de Andacollo, entre otros. Libardo es Ingeniero Civil Eléctrico de la Universidad de Concepción y obtuvo su experiencia trabajando en el Instituto de Investigaciones Tecnológicas de la Universidad de Concepción, JaakkoPöyry Chile, Flúor Daniel Chile, Industrial Support Company Limitada (ahora HighService) y más recientemente en Hatch.
	</p>
</div>