<div class="container container_boss">
	<a href="{{ route('team') }}" class="btn btn-outline-primary">atras</a>
	<div class="container_img">
		<img src="{{ asset('img/team/FYET.png') }}" class="boss_img" />
	</div>
	<br>
	<h1 class="boss_name">FRANCISCO YAÑEZ ESPINOSA</h1>
	<h3 class="boss_position">Gerente Legal</h3>
		<ul class="boss_social">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-instagram"></i></a></li>
		</ul>
	<p>
		Francisco es Abogado de la Universidad Alberto Hurtado, Magister en Derecho Económico y Recursos Naturales de la Universidad del Desarrollo y actualmente se encuentra cursando un Magister de Derecho de la Energía de la Universidad Mayor. Cuenta con un Diplomado en Libre Competencia en la Pontificia Universidad Católica de Chile y un Diplomado en Prevención, Detección e Investigación de Fraudes de la Facultad de Economía y Negocios de la Universidad de Chile. Se ha desempeñado dentro del sector energético, donde ha trabajado en Tinguirirca Energía, Ministerio de Energía, Empresa Nacional de Petroleo (ENAP) y Enel Green Power Chile y Países Andinos. Actualmente es el Gerente Legal del consorcio oEnergy – Sybac.
	</p>
</div>