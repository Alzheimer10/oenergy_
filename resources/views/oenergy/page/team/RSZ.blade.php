<div class="container container_boss">
	<a href="{{ route('team') }}" class="btn btn-outline-primary">atras</a>
	<div class="container_img">
		<img src="{{ asset('img/team/RSZT.png') }}" class="boss_img" />
	</div>
	<br>
	<h1 class="boss_name">RICARDO SYLVESTER ZAPATA</h1>
	<h3 class="boss_position">Gerente General</h3>
		<ul class="boss_social">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-instagram"></i></a></li>
		</ul>
	<p>
		Ricardo posee 30 años de experiencia en desarrollo y gerenciamiento de proyectos, mantenimiento industrial, energías renovables, administración de contratos y recursos humanos, seguimiento de presupuesto, indicadores de gestión operacional y financiera, orientación a control de costos, experiencia en desarrollo de proyectos en la industria de defensa, industria minera, industria de callcenter y la industria manufacturera y modelamiento financiero de proyectos de inversión en energías renovables. Ricardo posee una capacidad única para liderar, planificar y organizar equipos de trabajo, de carácter dinámico y entusiasta, orientado al cumplimiento de metas y objetivos. Ricardo es Ingeniero Civil Eléctrico de la Universidad de Concepción y su experiencia fue obtenida trabajando en Empresa Nacional de Aeronáutica (ENAER), Desarrollo de Tecnologías y Sistemas (DTS), Compañía Minera Disputada de Las Condes y DTS Call Center.
	</p>
</div>