<div class="container container_boss">
	<a href="{{ route('team') }}" class="btn btn-outline-primary">atras</a>
	<div class="container_img">
		<img src="{{ asset('img/team/YAST.png') }}" class="boss_img" />
	</div>
	<br>
	<h1 class="boss_name">Yuri Andrade Sylvester</h1>
	<h3 class="boss_position">Gerente de Negocios y Tecnologías</h3>
		<ul class="boss_social">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-instagram"></i></a></li>
		</ul>
	<p>
		Yuri posee 8 años de experiencia en el área de investigación y desarrollo de tecnologías, marketing técnico, ventas internacionales, desarrollo de negocios a través de estudios de mercado, gerenciamiento de proyectos y programas a gran escala, modelación financiera, importación y distribución, startups y gestión empresarial. Yuri posee dos patentes en el área de electrónica orgánica y fue responsable de la penetración de nuevos productos de semiconductores de Xradia en los mercados de Italia, Taiwán y Corea. Con su gran visión global, Yuri es un experto en visualizar oportunidades de negocio y formular soluciones integrales basadas en tecnologías innovadoras. Yuri es Ingeniero de Ciencias Materiales de la Universidad de Cornell en Estados Unidos y su experiencia la forjó trabajando en Intel Corporation, Intel Labs y Xradia Inc. (ahora Carl Zeiss X-RayMicroscopy) prestando servicios a compañías como Samsung, TSMC, Apple, Microsoft, entre otras. Yuri habla inglés y español.
	</p>
</div>