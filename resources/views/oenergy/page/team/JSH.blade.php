<div class="container container_boss">
	<a href="{{ route('team') }}" class="btn btn-outline-primary">atras</a>
	<div class="container_img">
		<img src="{{ asset('img/team/JSHT.png') }}" class="boss_img" />
	</div>
	<br>
	<h1 class="boss_name">JUAN PABLO SAN MARTÍN HERNÁNDEZ</h1>
	<h3 class="boss_position">Gerente de Estudios Eléctricos</h3>
		<ul class="boss_social">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-instagram"></i></a></li>
		</ul>
	<p>
		Juan Pablo es un emprendedor por excelencia, democratizador de las energías renovables no convencionales en Chile, Ingeniero Civil Electricista por	formación de la Universidad Técnica Federico Santa María. Más de 8 años de experiencia en el área de coordinación y administración de proyectos, con conocimientos técnicos en áreas relacionadas con sistemas eléctricos de potencia.
	</p>
	<p>
		Posee un perfil analítico, resolutivo, con habilidades de liderazgo, y con experiencia en el desarrollo de proyectos de energía renovable. Entusiasta de incorporar e implementar ERNC en Chile, promoviendo el desarrollo de proyectos de generación empleando tecnologías de fuente fotovoltaica y eólica principalmente sobre la base del emprendimiento como eje central.
	</p>
</div>