<div class="container container_boss">
	<a href="{{ route('team') }}" class="btn btn-outline-primary">atras</a>
	<div class="container_img">
		<img src="{{ asset('img/team/CSZT.png') }}" class="boss_img" />
	</div>
	<br>
	<h1 class="boss_name">Christian Sylvester Zapata</h1>
	<h3 class="boss_position">Gerente de Proyectos Solares</h3>
		<ul class="boss_social">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-instagram"></i></a></li>
		</ul>
	<p>Christian posee 24 años de experiencia, es Biólogo Marino de la Universidad de Concepción. Estudió y trabajó en el Departamento de Oceanografía de la misma universidad donde participó en diversos proyectos de investigación científica relacionados con circulación, ecología y ciclos biogeoquímicos en las costas de Chile y el Pacífico Sur-Oriental. Luego se desempeñó por varios años en la industria privada como consultor y gestor ambiental para empresas del rubro acuícola (salmonicultura y mitilicultura), que desarrollan su actividad en los canales y fiordos del sur de Chile, X y XI regiones.</p>
	<p>Durante su trayectoria profesional y por inquietud personal, se desempeñó por más de 5 años como monitor en programas de desarrollo social y técnico asesor en temas apícolas orientado a grupos de pequeños y medianos agricultores en diversas comunas de la provincia de Ñuble.
	Versátil, emprendedor, entusiasta, facilidad para desarrollar trabajos en conjunto y en equipo, posee altas capacidades para coordinar y ejecutar trabajos en terreno.</p>
	<p>Hoy promueve, gestiona y apoya el desarrollo de proyectos solares en la zona centro-sur de Chile.</p>
</div>