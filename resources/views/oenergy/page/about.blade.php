@extends('oenergy.layout.master')
@section('title', 'about')
@section('styles')
<!-- scripts for page:about -->
@stop

@section('content')
<!-- content for page:about -->
<div class="container container__main">
    <div class="container-fluid">
		<div class="row">
			<div class="col-xs-6">
				<h1 class="border__bottom">Misión</h1>
				<p>“La misión de oEnergy es crear rentabilidad operacional a sus clientes por medio de la implementación de soluciones individualizadas de generación eléctrica renovable y a su vez reducir la huella de carbono de nuestro planeta.”</p>
			</div>
			<div class="col-xs-6">
				<h1 class="border__bottom">Visión</h1>
				<p>“La visión de oEnergy es posicionarse como la empresa nacional líder de proyectos llave-en-mano y de tipo Energy Savings Company (ESCO) de generación eléctrica renovable con potencias entre 5 kW y 3 MW.”</p>
			</div>
		</div>
	</div>
</div>
<!-- content-end for page:about -->
@stop

<!-- scripts for page:about -->
@section('scripts')
@stop