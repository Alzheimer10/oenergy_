@extends('oenergy.layout.master')
@section('title', 'values')
<!-- scripts for page:values -->
@section('styles')
<style>
	.service-block { background-color: transparent; padding: 40px 20px; text-align: center; }
    .service-icon { height: 140px; width: 140px; text-align: center; background-color: #f8f9fa; border-radius: 100%; padding: 30px; margin-bottom: 20px; align-items: center; display: inline-block; }
    /*.service-block:hover { background-color: #f8f9fa; padding: 40px 20px; text-align: center; cursor: pointer; }*/
</style>
@stop

@section('content')
<!-- content for page:values -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom  ttu">@lang('app.nuestros valores')</h1>
        <p>@lang('app.texto_valores_1')</p>
    </div>
    <div class="container-fluid">
		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<!--Compromiso -->
					<div class="col-xs-6">
	                    <div class="service-block">
	                        <div class="service-icon">
	                            <img src="{{ asset('img/apreton-de-manos.svg') }}" alt="">
	                        </div>
	                        <div class="service-content">
	                            <h4>@lang('app.compromiso')</h4>
								<p>@lang('app.texto_valores_2')</p>
	                        </div>
	                    </div>
					</div>
					<!--Compromiso-end -->

					<!--Liderazgo -->
					<div class="col-xs-6">
                   		<div class="service-block">
	                        <div class="service-icon">
	                            <img src="{{ asset('img/conferencia.svg') }}" alt="">
	                        </div>
	                        <div class="service-content">
	                             <h4>@lang('app.liderazgo')</h4>
								<p>@lang('app.texto_valores_3')</p>
	                        </div>
	                    </div>
					</div>
					<!--Liderazgo-end -->
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="row">
					<!--Integridad -->
					<div class="col-xs-6">
	                    <div class="service-block">
	                        <div class="service-icon">
	                            <img src="{{ asset('img/conexion.svg') }}" alt="">
	                        </div>
	                        <div class="service-content">
	                             <h4>@lang('app.integridad')</h4>
								<p>@lang('app.texto_valores_4')</p>
	                        </div>
	                    </div>
					</div>
					<!--Integridad-end -->

					<!--Satisfacción -->
					<div class="col-xs-6">
	                    <div class="service-block">
	                        <div class="service-icon">
	                            <img src="{{ asset('img/crecer.svg') }}" alt="">
	                        </div>
	                        <div class="service-content">
	                            <h4>@lang('app.satisfacción')</h4>
								<p>@lang('app.texto_valores_5')</p>
	                        </div>
	                    </div>
					</div>
					<!--Satisfacción-end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- content-end for page:values -->
@stop

@section('scripts')
<!-- scripts for page:values -->
@stop