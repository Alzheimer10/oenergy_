@extends('oenergy.layout.master')
@section('title', 'blog')
@section('styles')
<!-- scripts for page:blog -->
<style>
.size-1of4{
	width: calc( 100% / 3);
}
</style>
@stop

@section('content')
<!-- content for page:blog -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom">BLOG</h1>
    </div>
    <div class="container-fluid" id="grid" data-columns="3">
			
		<!-- Net Billing -->
		<figure class="card__blog">
		  <img src="{{ asset('img/posts/post_01.jpg') }}" width="100%" alt="image_post_net_billing"/>
		  <div class="date"><span class="day">20</span><span class="month">May</span></div><i class="fa fa-search-plus" aria-hidden="true"></i>
		  <figcaption>
		    <h3>Net Billing</h3>
		    <p>Gracias al Netbilling se puede aprovechar al máximo la producción del sistema solar para cubrir parte del consumo eléctrico y reducir los costos de electricidad.</p>
		    <button>Leer más</button>
		  </figcaption><a href="#"></a>
		</figure>
		<!-- Net Billing-END -->

		<!-- Solar Tracking-END -->
		<figure class="card__blog">
		  <img src="{{ asset('img/posts/post_02.jpg') }}" width="100%" alt="image_post_Solar_Tracking"/>
		  <div class="date"><span class="day">20</span><span class="month">May</span></div><i class="fa fa-search-plus" aria-hidden="true"></i>
		  <figcaption>
		    <h3>Solar Tracking</h3>
		    <p>Un seguidor solar es un dispositivo mecánico capaz de orientar los paneles solares de forma que éstos permanezcan aproximadamente perpendiculares a los rayos solares..</p>
		    <button>Leer más</button>
		  </figcaption><a href="#"></a>
		</figure>
		<!-- Solar Tracking-END -->

		<!-- Recurso ERNC-END -->
		<figure class="card__blog">
		  <img src="{{ asset('img/posts/post_03.jpg') }}" width="100%" alt="image_post_Recurso_ERNC"/>
		  <div class="date"><span class="day">20</span><span class="month">May</span></div><i class="fa fa-search-plus" aria-hidden="true"></i>
		  <figcaption>
		    <h3>Recurso ERNC</h3>
		    <p>En Chile, se han planteado incentivos para aumentar el uso de fuentes de energía renovable por sobre los combustibles fósiles...</p>
		    <button>Leer más</button>
		  </figcaption><a href="#"></a>
		</figure>
		<!-- Recurso ERNC-END -->

	</div>
</div>
<!-- content-end for page:blog -->
@stop

@section('scripts')
<!-- scripts for page:blog -->
    <script type="text/javascript" src="{{ asset('modules/salvattore/salvattore.min.js') }}"></script>
@stop