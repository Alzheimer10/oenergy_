<ul>
	<li>Para proyectos sobre 500 kW, oEnergy financiará proyectos por medio de una ESCO (servicio de atracción de inversionistas)</li>
	<li>Asesoria técnica en negociación de contratos de compra de energía</li>
	<li>Gestión de postulación de proyectos para fondos concursales de subsidio (CORFO, CER, CNR, AChEE, FNDR, FIC, FIA, Mercado Publico, etc.)</li>
	<li>Brokerage de bienes raíces de plantas generadoras</li>
</ul>