<div class="panel-group" id="accordion">

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
			  <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
			  	AUDITORÍA TÉCNICA DE INSTALACIONES EXISTENTES
			  </a>
			</h4>
		</div>
		<div id="collapse1" class="panel-collapse collapse in">
			<div class="panel-body">
				<p>Contamos con un equipo de ingenieros especialistas en inspección y auditoría técnica de instalaciones en baja tensión.</p>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
			  <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
			  	MONITOREO Y ANÁLISIS DE REDES
			  </a>
			</h4>
		</div>
		<div id="collapse2" class="panel-collapse collapse">
			<div class="panel-body">
				<p>Análisis y monitoreo de redes eléctricas, lo que permite supervisar, diagnosticar y  resolver problemas de rendimiento de la red, así como entregar herramientas de gestión de la demanda.</p>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
			  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
				MEDICIÓN DE RESISTIVIDAD Y DISEÑO DE MALLAS DE PUESTA A TIERRA
			  </a>
			</h4>
		</div>
		<div id="collapse3" class="panel-collapse collapse">
			<div class="panel-body">
				<p>Hacemos el sondeo eléctrico del terreno, diseño de la malla de puesta a tierra y posteriormente su medición de resistencia.</p>
			</div>
		</div>
	</div>
</div>