<h3>Técnico-Económico-Legal de Proyectos de Generación</h3>
<ul>
	<li>Estudio de evaluación de impacto ambiental (DIA y EIA)</li>
	<li>Estudios eléctricos de factibilidad de conexión a la red</li>
	<li>Estudios de dimensionamiento de recurso natural (radiación, viento, corrientes, hidráulicos)</li>
	<li>Estudios de eficiencia energética, modelación tarifaria, medición y verificación</li>
	<li>Estudios geológicos, topográficos, hidrológicos, entre otros</li>
	<li>Estudios legales de títulos de propiedad, derechos de agua, uso de suelo, servidumbre, etc.</li>
</ul>