@extends('oenergy.layout.master')
@section('title', 'services')
<!-- scripts for page:services -->
@section('styles')
	<style>
		.panel-title a {
		    display: block;
		    padding: 10px 15px;
		    text-decoration: none;
		}
		.panel-default > .panel-heading {
		    margin: auto;
		    color: #333;
		    border-bottom: 2px solid #2380c8;
		    border-radius: 0;
		    padding: 0px;

		}
	</style>
@stop

@section('content')
<!-- content for page:services -->
<div class="container container__main">
    <div class="container-fluid">
		<!--PEN CODE-->
		<div class="row">
			@if($service == 'estudios_sistemas_electricos')

				<h1 class="border__bottom">@lang('app.estudios_sistemas_electricos')</h1>
				@if( session('lang')=='es')
					@include('oenergy.page.services.es.estudios_sistemas_electricos')
				@else
					@include('oenergy.page.services.en.estudios_sistemas_electricos')
				@endif

			@elseif($service == 'operacion_mantención')

				<h1 class="border__bottom">@lang('app.operacion_mantención')</h1>
				@if( session('lang')=='es')
					@include('oenergy.page.services.es.operacion_mantencion')
				@else
					@include('oenergy.page.services.en.operacion_mantencion')
				@endif

			@elseif($service == 'factibilidad')

				<h1 class="border__bottom">@lang('factibilidad')</h1>
				@if( session('lang')=='es')
					@include('oenergy.page.services.es.factibilidad')
				@else
					@include('oenergy.page.services.en.factibilidad')
				@endif

			@elseif($service == 'due_diligence')

				<h1 class="border__bottom">@lang('app.due_diligence')</h1>
				@if( session('lang')=='es')
					@include('oenergy.page.services.es.due_diligence')
				@else
					@include('oenergy.page.services.en.due_diligence')
				@endif

			@elseif($service == 'servicios_terreno')

				<h1 class="border__bottom">@lang('app.servicios_terreno')</h1>
				@if( session('lang')=='es')
					@include('oenergy.page.services.es.servicios_terreno')
				@else
					@include('oenergy.page.services.en.servicios_terreno')
				@endif

			@elseif($service == 'financiamiento_subsidios')

				<h1 class="border__bottom">@lang('app.financiamiento_subsidios')</h1>
				@if( session('lang')=='es')
					@include('oenergy.page.services.es.financiamiento_subsidios')
				@else
					@include('oenergy.page.services.en.financiamiento_subsidios')
				@endif

			@elseif($service == 'construccion')

				<h1 class="border__bottom">@lang('construccion')</h1>
				@if( session('lang')=='es')
					@include('oenergy.page.services.es.construccion')
				@else
					@include('oenergy.page.services.en.construccion')
				@endif

			@elseif($service == 'diseño_desarrollo')

				<h1 class="border__bottom">@lang('app.diseño_desarrollo')</h1>
				@if( session('lang')=='es')
					@include('oenergy.page.services.es.diseno_desarrollo')
				@else
					@include('oenergy.page.services.en.diseno_desarrollo')
				@endif

			@endif
		</div>
	</div>
</div>

@endsection
