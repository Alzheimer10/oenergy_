<p>El promotor de un nuevo proyecto, debe cumplir con una serie de requisitos técnicos para lograr la aprobación de la incorporación de la nueva instalación al sistema interconectado, las cuales están sujetan a que se preserven la calidad de servicio y las condiciones de seguridad a las personas y equipos.</p>

<p>Los estudios que realizan nuestros profesionales especializados, están sustentados en la experiencia de la aplicación de los criterios de las normas técnicas, conocimiento de las leyes y regulaciones, más el manejo en las evaluaciones con las herramientas de simulación, los cuales se pueden dividir en las siguientes categorías:</p>

<div class="panel-group" id="accordion">

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
			  <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
			  ESTUDIOS DE PREFACTIBILIDAD</a>
			</h4>
		</div>
		<div id="collapse1" class="panel-collapse collapse">
			<div class="panel-body">
			Enfocados a determinar condiciones o limitaciones de las redes candidatas a conectarse, así como identificación de puntos de conexión para el proyecto, cálculo de holguras de capacidad disponible en transmisión existente para inyección del proyecto, dimensionamiento y diseño conceptual de solución de conexión.
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
			  <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
			  ESTUDIOS DE IMPACTO SISTÉMICO</a>
			</h4>
		</div>
		<div id="collapse2" class="panel-collapse collapse">
		  	<div class="panel-body">
				<p>oEnergy realiza estudios eléctricos que permiten determinar los efectos que producirá la operación de una nueva instalación sobre los sistemas eléctricos evaluando el impacto en la zona de repercusión.</p>
				<div class="panel-group" id="accordion2">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							  <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2_1">
							  Estudio de Cortocircuito y Verificación de Capacidad de Ruptura de Interruptores</a>
							</h4>
						</div>
						<div id="collapse2_1" class="panel-collapse collapse">
						  	<div class="panel-body">
						  	<p>Este estudio corresponde a un análisis técnico sobre el efecto que produce la nueva instalación en el respectivo SI.</p>
							<p>Su objetivo es efectuar una comparación entre los niveles previos y posteriores al ingreso de la nueva instalación, contrastando los resultados con las capacidades de ruptura informadas por el CDEC en su página web.</p>
							<p>Se considerarán las condiciones más exigentes usando para la determinación del nivel de cortocircuito el método definido por la DO de CDEC y la NTSyCS, mediante la simulación en DIgSILENT, con la incorporación del proyecto en la base de datos del CDEC-SIC.</p>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							  <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2_2">
							  Estudio de Impacto Evaluación en Régimen Permanente</a>
							</h4>
						</div>
						<div id="collapse2_2" class="panel-collapse collapse">
						  	<div class="panel-body">
						  	<p>El estudio busca evaluar que ante la incorporación de la nueva instalación de generación se cumpla con los requerimientos impuestos en la NT de SyCS, en condiciones normales de operación del SI y operación con instalaciones con mantenimiento programado.</p>
							<p>Su objetivo es demostrar en base a simulaciones de flujos de potencia, que la incorporación de la nueva instalación no pone en riesgo la operación del sistema, para ninguna situación normal o de contingencia y corroborar no se vean superadas las capacidades térmicas o limitaciones de transferencia por las líneas, y márgenes de seguridad establecidos para los distintos niveles de tensión.</p>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							  <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2_3">
							  Estudio de Estabilidad Transitoria</a>
							</h4>
						</div>
						<div id="collapse2_3" class="panel-collapse collapse">
						  	<div class="panel-body">
						  	<p>Se evalúa el impacto que provoca en el SI, en sus variables: frecuencia, tensión, ángulos, flujos, amortiguamiento, etc., la incorporación del nuevo proyecto de generación.</p>
							<p>Se trata de una evaluación dinámica, de análisis en el tiempo, que contempla diversas contingencias, incluyendo aquellas aplicadas directamente sobre las nuevas instalaciones.</p>
							<p>Su objetivo es evaluar el desempeño del SI, y cuantificar el impacto que provoca el ingreso de las nuevas instalaciones.
							<p>Verificar si el nivel de amortiguamiento de las oscilaciones electromagné-ticas, tensiones y frecuencias en barras y ángulo de rotor de las máquinas síncronas, cumplen con los márgenes de operación establecidos en las disposiciones de la NTSyCS.</p>
							<p>Considera diversas contingencias, mediante la observación de la evolución en el tiempo de las variables de: frecuencia, tensión y oscilaciones electromagnéticas de los flujos de potencia por las líneas de transmisión.</p>
							</div>
						</div>
					</div>
				</div>
		  	</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
		    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
		    ESTUDIOS DE COORDINACIÓN Y AJUSTES DE PROTECCIONES</a>
		  </h4>
		</div>
		<div id="collapse3" class="panel-collapse collapse">
		  	<div class="panel-body">
			  	<p>El Estudio de Coordinación de Protecciones consiste en realizar la coordinación del esquema de protecciones de las líneas de transmisión de alta tensión, entre las distintas subestaciones relacionadas a la interconexión del proyecto según la filosofía de protecciones desarrollada por el cliente.</p>
				<p>Su objetivo es entregar los criterios de coordinación y cálculos de los ajustes de las protecciones de los equipos del proyecto, y las instalaciones que se ven afectadas por la incorporación del proyecto.</p>
				<p>Considera la realización de: Estudio de Cortocircuito, Estudio de Variables Eléctricas y Cálculo de Impedancias Aparentes.
				<p>Los resultados de los estudios vinculados a los criterios de ajustes resultan en los valores a ser ingresados en las protecciones, estas conclusiones son respaldadas con los diagramas R-X; T-I y T-D correspondientes.</p>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
		    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
		    ESTUDIOS DE ENERGIZACIÓN DE TRANSFORMADORES</a>
		  </h4>
		</div>
		<div id="collapse4" class="panel-collapse collapse">
		  <div class="panel-body">
			<p>El objetivo del estudio es evaluar el impacto que provocará en el Sistema Interconectado, la energización del transformador de poder del nuevo generador, analizando para ello los estados transitorios electromagnéticos de operación en el sistema eléctrico.</p>
		  </div>
		</div>
	</div>
	
</div>