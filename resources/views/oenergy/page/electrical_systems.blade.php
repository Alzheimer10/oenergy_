@extends('oenergy.layout.master')
@section('title', 'electical_systems')
@section('styles')
<!-- scripts for page:electical_systems -->
@stop

@section('content')
<!-- content for page:electical_systems -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom">PREGUNTAS FRECUENTES</h1>
        <p>Se ofrece aquí respuesta a una selección de las preguntas más frecuentes planteadas por los usuarios del servicio de consultas lingüísticas.</p>
    </div>
</div>
<!-- content-end for page:electical_systems -->
@stop

@section('scripts')
<!-- scripts for page:electical_systems -->
@stop