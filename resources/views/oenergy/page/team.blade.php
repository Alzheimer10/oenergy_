@extends('oenergy.layout.master')
@section('title', 'team')
<!-- scripts for page:team -->
@section('styles')
@stop

@section('content')
<!-- content for page:team -->
<div class="container container__main">
	@if(!isset($boss))
	    <div class="container-fluid">
	        <h1 class="border__bottom">NUESTROS EQUIPO</h1>
	    </div>
	    <div class="container-fluid">
			<div class="row team row-centered">
				<!-- Christian Sylvester Zapata -->
				<card-boss boss="{{ route('team','CSZ') }}" name="Christian" lastname="Sylvester Zapata" position="Gerente de Proyectos Solares" img="{{ asset('img/team/CSZT.png') }}" ></card-boss>
				<!-- END | Christian Sylvester Zapata -->

				<!-- Yuri Andrade Sylvester -->
				<card-boss boss="{{ route('team','YAS') }}" name="Yuri" lastname="Andrade Sylvester" position="Gerente de Negocios y Tecnologías" img="{{ asset('img/team/YAST.png') }}" ></card-boss>
				<!-- END | Yuri Andrade Sylvester -->

				<!-- Francisco Yáñez Espinosa -->
				<card-boss boss="{{ route('team','FYE') }}" name="Francisco" lastname="Yáñez Espinosa" position="Gerente Legal" img="{{ asset('img/team/FYET.png') }}" ></card-boss>
				<!-- END | Francisco Yáñez Espinosa -->

				<!-- Juan Pablo an Martín Hernández -->
				<card-boss boss="{{ route('team','JSH') }}" name="Juan Pablo" lastname="San Martín Hernández" position="Gerente de Estudios Eléctricos" img="{{ asset('img/team/JSHT.png') }}" ></card-boss>
				<!-- END | Juan Pablo an Martín Hernández -->

				<!-- Libardo Hernández Fernández -->
				<card-boss boss="{{ route('team','LHF') }}" name="Libardo" lastname="Hernández Fernández" position="Gerente de Ingeniería y Construcción" img="{{ asset('img/team/LHFT.png') }}"></card-boss>
				<!-- END | Libardo Hernández Fernández -->

				<!-- Ricardo Sylvester Zapata -->
				<card-boss boss="{{ route('team','RSZ') }}" name="Ricardo" lastname="Sylvester Zapata" position="Gerente de Estudios Eléctricos" img="{{ asset('img/team/RSZT.png') }}" ></card-boss>
				<!-- END | Ricardo Sylvester Zapata -->

				<!-- Miguel Cáceres Peralta -->
				<card-boss boss="{{ route('team','MCP') }}" name="Miguel" lastname="Cáceres Peralta" position="Gerente de Proyectos Solares" img="{{ asset('img/team/MCPT.png') }}" ></card-boss>
				<!-- END | Miguel Cáceres Peralta -->
			</div>
		</div>
	@else
		@if($boss=="CSZ")
			@include('oenergy.page.team.CSZ')
		@elseif($boss=="YAS")
			@include('oenergy.page.team.YAS')
		@elseif($boss=="JSH")
			@include('oenergy.page.team.JSH')
		@elseif($boss=="FYE")
			@include('oenergy.page.team.FYE')
		@elseif($boss=="LHF")
			@include('oenergy.page.team.LHF')
		@elseif($boss=="RSZ")
			@include('oenergy.page.team.RSZ')
		@elseif($boss=="MCP")
			@include('oenergy.page.team.MCP')
		@endif
	@endif
</div>
<!-- content-end for page:team -->
@stop

@section('scripts')
<!-- scripts for page:team -->
	<script type="text/x-template" id="checkbox-template">
		<div class="checkbox-wrapper" @click="check">
			<div :class="{ checkbox: true, checked: checked }"></div>
			<div class="title"></div>
		</div>
	</script>

@stop