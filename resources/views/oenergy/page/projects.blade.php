@extends('oenergy.layout.master')
@section('title', 'projects')
<!-- scripts for page:projects -->
@section('styles')
  <link rel="stylesheet" href="{{ asset('js/fixed-sticky/fixedsticky.css') }}">
  <style>
    .fixedsticky { top: 0; }
    .container__main{ overflow: inherit; }
  </style>
@stop

@section('content')
<!-- content for page:projects -->
<div class="container container__main" id="projects">
    <div class="container-fluid">
        <h1  class="border__bottom">NUESTROS PROYECTOS</h1>
        <p>Somos una empresa formada por un equipo multidisciplinario que se enfoca en proyectos llave-en-mano y de tipo Energy Savings Company (ESCO) de generación eléctrica renovable con potencias entre 2,5KW y 3MW.</p>
    </div>

    <div class="container">
      {{-- <div class="top fixedsticky"> --}}
      <div class="row">
        <div class=" hidden-xs col-sm-6 col-md-3 fixedsticky" id="container-svg">
            <div>
              @include('oenergy.page.2map_svg')
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-9">
          <div class="content_accordion">
                <div class="panel-group" id="accordion" style="padding-top: 20px">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_turcas" onclick="mouseClick('turcas',this)" class="link wipe">
                                  <span><b>Las Turcas</b>, Comuna Melipilla Sector Popeta RM</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_turcas" class="panel-collapse collapse">
                            <div  class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/08.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_queltehue" onclick="mouseClick('queltehue',this)" class="link wipe">
                                  <span><b>El Queltehue</b>, Sector Bollenar Comuna Melipilla RM</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_queltehue" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/04.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <!-- El Pilpen, Sector y Comuna Maria Pinto RM -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_pilpe" onclick="mouseClick('pilpe',this)" class="link wipe">
                                  <span><b>El Pilpen</b>, Sector y Comuna Maria Pinto RM</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_pilpe" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/08.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- (Provincia_Biobio) El Cernicalo, Comuna Ninhue Región Del Biobio -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_cernicalo" onclick="mouseClick('cernicalo',this)" class="link wipe">
                                  <span><b>El Cernicalo</b>, Comuna Ninhue Región Del Biobio</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_cernicalo" class="panel-collapse collapse Provincia_Biobio">
                            <div class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/04.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- (Provincia_Linares) Los Gorriones, sector retiro comuna parral región Maule -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_gorriones" onclick="mouseClick('gorriones',this)" class="link wipe">
                                  <span><b>Los Gorriones</b>, sector retiro comuna parral región Maule</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_gorriones" class="panel-collapse collapse Provincia_Linares">
                            <div class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/04.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- (Provincia_Melipilla) El pitio, Comuna Longaví Región Maule -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_pitio" onclick="mouseClick('pitio',this)" class="link wipe">
                                  <span><b>El pitio</b>, Comuna Longaví Región Maule</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_pitio" class="panel-collapse collapse Provincia_Linares">
                            <div class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/04.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- (Region_Maule) Los patos, Comuna Panguilemo Region Maule -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_patos" onclick="mouseClick('patos',this)" class="link wipe">
                                  <span><b>Los patos</b>, Comuna Panguilemo Region Maule</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_patos" class="panel-collapse collapse Region_Maule">
                            <div class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/04.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- (Region_Bio_Bio) El Chincol, Comuna Ñiquen Region del Biobio -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_chincol" onclick="mouseClick('chincol',this)" class="link wipe">
                                  <span><b>El Chincol</b>, Comuna Ñiquen Region del Biobio</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_chincol" class="panel-collapse collapse Region_Biobio">
                            <div class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/04.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- (Region_OHiggins) El Picurio, Comuna de Chimbarongo Region de O´Higgins -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#panel_body_picurio" onclick="mouseClick('picurio',this)" class="link wipe">
                                  <span><b>El Picurio</b>, Comuna de Chimbarongo Region de O´Higgins</span></a>
                            </h4>
                        </div>
                        <div id="panel_body_picurio" class="panel-collapse collapse Region_OHiggins">
                            <div class="panel-body">
                                <div class="header"><img src="{{ asset('img/projects/04.jpeg') }}" alt="08_thumbnail" width="100%" height="auto"></div>
                                <div class="panel-content">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod libero impedit dolorum iure. Eos sint autem nostrum culpa quas eius, quidem animi laborum ratione cupiditate. Aspernatur minus vitae sint totam!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- content-end for page:projects -->
@stop

@section('scripts')
  <script src="{{ asset('js/fixed-sticky/fixedsticky.js') }}"></script>
<!-- scripts for page:projects -->
  <script type="text/javascript">
        function mouseClick(parque,elem) {
          if( !$('#m_'+parque).hasClass('in')){
            $('.marcador').removeClass('in');
          }
            if($(elem).hasClass('wipe')){

              if(!$(elem).hasClass('wipe-collapsed')){
                $('.wipe').removeClass('wipe-collapsed');
                $(elem).toggleClass('wipe-collapsed');
              }else
                $('.wipe').removeClass('wipe-collapsed');
            }

          $('#m_'+parque).toggleClass('in');
        };
    </script>
@stop
