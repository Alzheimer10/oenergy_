{{-- Stored in resources/views/oenergy/layouts/app.blade.php --}}
<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <title>{{ Config::get('app.name') }} | @yield('title') </title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <!-- METAS -->
      <meta name="description" content="Oenergy ">
      <meta name="keywords" content="oEnergy, llave-en-mano, ESCO ,  generación eléctrica renovable , Energía solar sin barreras, Suministro eléctrico competitivo">
      <meta name="author" content="Carlos Anselmi | carlosanselmi2@gmail.com - {{ Config::get('app.name') }}">
      <meta property="og:title" content="{{ Config::get('app.name') }}">
      <meta property="og:type" content="website">
      <meta name="viewport" content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'>
      <meta name="MobileOptimized" content="width">
      <meta name="HandheldFriendly" content="true">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="crsf-token" content="{{ csrf_token() }}">
      <script> window.Laravel = { csrfToken: '{{ csrf_token() }}' } </script>
    <!-- FONTS -->
      <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

    <!-- STYLES -->
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap.css') }}">
      <!-- Main CSS -->
      <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
      <!-- bootstrap-dropdownhover -->
      {{-- <link rel="stylesheet" href="{{ asset('modules/bootstrap-dropdown-hover/css/bootstrap-dropdownhover.min.css') }}"> --}}
      <!-- animate -->
      <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}" />
      <!-- Fontawesome CSS -->
      <link rel="stylesheet" type="text/css" href="{{ asset('modules/font-awesome/css/font-awesome.min.css') }}">
      {{-- <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.0.2/css/all.css"> --}}
    
    @yield('styles')
  </head>
  <body>
    
    <div id="appvue">
      <!-- navbar -->
      @include('oEnergy.components.navbar')
      <!-- navbar-end -->

      <!-- content -->
      @yield('content')
      <!-- content-end -->

      <!-- footer -->
      @include('oEnergy.components.footer')
      <!-- footer-end -->
      <div class="scroll-top-wrapper">
        <i class="fa fa-2x fa-chevron-up"></i>
      </div>

    </div>

    <!-- scripts_main -->
      <!-- jquery -->
      <script type="text/javascript" src="{{ asset('modules/jquery/jquery-3.1.1.min.js') }}"></script>
      <!-- bootstrap -->
      <script type="text/javascript" src="{{ asset('modules/bootstrap/bootstrap.min.js') }}"></script>
      <!-- bootstrap-dropdown-hover -->
      {{-- <script type="text/javascript" src="{{ asset('modules/bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js') }}"></script> --}}
      <!-- lazyload -->
      <script type="text/javascript" src="{{ asset('js/lazyload.min.js') }}"></script>
      <!-- magnific-popup -->
      <script type="text/javascript" src="{{ asset('modules/jquery.magnific-popup/jquery.magnific-popup.min.js') }}"></script>
      <!-- scrollreveal -->
      <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
      <!-- Main -->
      <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
    <!-- scripts_main-end -->
    @yield('scripts')
  </body>
</html>
