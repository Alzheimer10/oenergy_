@extends('oenergy.layout.master')

@section('title', 'home')

@section('styles')
<!-- styles for page:index -->
	<link rel="stylesheet" href="{{ asset('modules/jquery.magnific-popup/magnific-popup.min.css') }}">
	<style> .lib-img__before{display: none;} .section .row{overflow: hidden;} </style>
@stop

@section('content')
<!-- carousel-home -->
	<div id="carousel-home" class="carousel slide carousel-fade carousel-animate carousel-bg" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
	      <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
	      <li data-target="#carousel-home" data-slide-to="1"></li>
	      <li data-target="#carousel-home" data-slide-to="2"></li>
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
	     <div class="item active" style="background-image: url({{ asset('img/carousel/img_carousel_1.jpg') }})">
	       <div class="carousel-caption">
	         <div class="hero">
	         	<hgroup class="zoomInDown animated">
	             <h1 class="fadeInLeft animated">@lang('app.carousel_1_titulo')</h1>
	             <h5 class="fadeInUp animated">@lang('app.carousel_1_texto')</h5>
	           </hgroup>
	           {{-- <button class="btn btn-hero btn-lg bounceInUp animated" role="button">SERVICIOS</button> --}}
	           <button class="btn btn-hero btn-lg bounceInUp animated" role="button"><a href="{{ route('contact') }}">@lang('app.contáctenos')</a></button>
	         </div>
	       </div>
	     </div>

	     <div class="item" style="background-image: url({{ asset('img/carousel/img_carousel_2.jpg') }})">
	       <div class="carousel-caption">
	         <div class="hero">
	         	<hgroup class="zoomInDown animated">
	             <h1 class="fadeInUp animated">@lang('app.carousel_2_titulo')</h1>
	             <h5 class="fadeInUp animated">@lang('app.carousel_2_texto')</h5>
	           </hgroup>
	           <button class="btn btn-hero btn-lg bounceInUp animated" role="button"><a href="{{ route('services') }}">@lang('app.nuestros servicios')</a></button>
	         </div>
	       </div>
	     </div>

	     <div class="item" style="background-image: url({{ asset('img/carousel/img_carousel_3.jpg') }})">
	       <div class="carousel-caption">
	         <div class="hero">
	         	<hgroup class="zoomInDown animated">
	             <h1 class="fadeInUp animated">@lang('app.carousel_3_titulo')</h1>
	             <h5 class="fadeInUp animated">@lang('app.carousel_3_texto')</h5>
	           </hgroup>
	           <button class="btn btn-hero btn-lg bounceInUp animated" role="button"><a href="{{ route('contact') }}">@lang('app.asesorias')</a></button>
	         </div>
	       </div>
	     </div>
		</div>
		
		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
		 <span class="glyphicon glyphicon-chevron-left fa fa-angle-left" aria-hidden="true"></span>
		</a>
		<a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
		 <span class="glyphicon glyphicon-chevron-right fa fa-angle-right" aria-hidden="true"></span>
		</a>
	</div>
<!-- carousel-home-end -->

<!-- content section:1-->
	<div class="container section section__1">
		<div class="row">
			<div class="col-xs-12 col-sm-12 dt-v ph-xs-5">
	        	<section class="sr_left">
	        		<h1  class="border__bottom">@lang('app.section_1_titulo')</h1>
	        		<p><span class="nbsp">&nbsp;</span>@lang('app.section_1_texto')</p>
	        		<a href="{{ route('services') }}" class="btn btn-outline-primary">@lang('app.nuestros servicios')</a>
	        	</section>
				<img src="{{ asset('img/energy_index.jpg') }}" alt="energy_index" width="100%" class="hidden-xs sr_right">
			</div>
		</div>
	</div>
<!-- content-end section:1 -->

<!-- content section:2 -->
	<div class="container section section__2">
		<div class="row">
			<div class="col-xs-12 justify-content-center js--fadeInUp sr_left" style="padding: 10px">
			 	<h1 class="border__bottom">@lang('app.section_2_titulo')</h1>
			  	<p><span class="nbsp">&nbsp;</span>@lang('app.section_2_texto')</p>
			</div>
		</div>

		<div class="row sr_bottom">

			<div class="col-xs-4 col-sm-4 col-lg-4">
			    <div class="lib-panel">
					<h3 class="border__bottom__10">@lang('app.section_2_subtitulo_1')</h3>
			        <p class="lib-row lib-desc">@lang('app.section_2_texto_1')</p>
			    </div>
			</div>

			<div class="col-xs-4 col-sm-4 col-lg-4">
				<div class="lib-panel">
					<h3 class="border__bottom__10">@lang('app.section_2_subtitulo_2')</h3>
					<p class="lib-row lib-desc">@lang('app.section_2_texto_2')</p>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-lg-4">
				<div class="lib-panel">
					<h3 class="border__bottom__10">@lang('app.section_2_subtitulo_4')</h3>
					<p class="lib-row lib-desc">@lang('app.section_2_texto_4')</p>
				</div>
			</div>
			<div class="container">
				<div class="col-xs-12">
				  <a href="#" class="btn btn-outline-primary pull-right">@lang('ver más')</a>
				</div>
			</div>
		</div>
	</div>
<!-- content-end section:2-->

<!-- content section:3 -->
	<div class="container section section_3">
		<div class="row">
			<div class="col-xs-12 col-lg-6 sr_left">
			  	<h1 class="border__bottom">@lang('app.section_3_titulo')</h1>
			  	<p>@lang('app.section_3_texto')</p>

			  	<a href="{{ route('projects') }}" class="btn btn-outline-primary pull-right">@lang('app.ver más')</a>
			</div>
			<div class="col-xs-12 col-lg-6 sr_right">
				<div data-columns="3">
		    		<div class="image_column size-1of3">
		          		<a href="{{ asset('img/projects/01.jpeg') }}">
		          			<img class="shadow" src="{{ asset('img/projects/01_thumbnail.jpg') }}"	alt="01_thumbnail.jpg"/>
		          		</a>
		          		<a href="{{ asset('img/projects/04.jpeg') }}">
		          			<img class="shadow" src="{{ asset('img/projects/04_thumbnail.jpg') }}"	alt="04_thumbnail.jpg"/>
		          		</a>
		    		</div>
		    		<div class="image_column size-1of3">
		          		<a href="{{ asset('img/projects/07.jpeg') }}">
		          			<img class="shadow" src="{{ asset('img/projects/07_thumbnail.jpg') }}"	alt="07_thumbnail.jpg"/>
		          		</a>
		          		<a href="{{ asset('img/projects/05.jpeg') }}">
		          			<img class="shadow" src="{{ asset('img/projects/05_thumbnail.jpg') }}"	alt="05_thumbnail.jpg"/>
		          		</a>
		    		</div>
		    		<div class="image_column size-1of3">
		          		<a href="{{ asset('img/projects/02.jpeg') }}">
		          			<img class="shadow" src="{{ asset('img/projects/02_thumbnail.jpg') }}"	alt="02_thumbnail.jpg"/>
		          		</a>
		          		<a href="{{ asset('img/projects/03.jpeg') }}">
		          			<img class="shadow" src="{{ asset('img/projects/03_thumbnail.jpg') }}"	alt="03_thumbnail.jpg"/>
		          		</a>
		    		</div>
				</div>
			</div>
		</div>
	</div>
<!-- content-end section:3-->

@stop

@section('scripts')
    <script>
   //      window.onload = function() {
			// $('.image_column a').magnificPopup({
			// 	type: 'image',
			// 	gallery: {
			// 	  enabled: true
			// 	}
			// });
   //      	$("img.lazyload").lazyload();
   //      }
    </script>
@stop