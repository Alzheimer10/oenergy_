@extends('oenergy.layout.master')
@section('title', '404')
@section('styles')
<!-- scripts for page:fap -->
@stop

@section('content')
<!-- content for page:fap -->
<div class="container container__main">
    <div class="container-fluid">
        <h1  class="border__bottom">@lang('app.404')</h1>
        <p>@lang('app.404_text_1')</p>
    </div>
</div>
<!-- content-end for page:fap -->
@stop

@section('scripts')
<!-- scripts for page:fap -->
@stop