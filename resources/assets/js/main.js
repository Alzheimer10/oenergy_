$(document).ready(function(){

  $(function(){
      $(document).on( 'scroll', function(){
        if ($(window).scrollTop() > 300) {
        $('.scroll-top-wrapper').addClass('scroll-show');
      } else {
        $('.scroll-top-wrapper').removeClass('scroll-show');
      }
    });
    $('.scroll-top-wrapper').on('click', scrollToTop);
  });
   
  function scrollToTop() {
    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
    element = $('body');
    offset = element.offset();
    offsetTop = offset.top;
    $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
  }

  window.sr = ScrollReveal();
  sr.reveal('.sr_right',  { mobile: false, reset: true, distance: '100px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)', origin: 'right'  , opacity: 0, duration: 1000, delay: 0 });
  sr.reveal('.sr_left',   { mobile: false, reset: true, distance: '100px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)', origin: 'left'   , opacity: 0, duration: 1000, delay: 0 });
  sr.reveal('.sr_top',    { mobile: false, reset: true, distance: '100px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)', origin: 'top'    , opacity: 0, duration: 1000, delay: 0 });
  sr.reveal('.sr_bottom', { mobile: false, reset: true, distance: '100px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)', origin: 'bottom' , opacity: 0, duration: 1000, delay: 0 });
  
});