<?php

return [

    /*
    |--------------------------------------------------------------------------
    | LINEAS DE TEXTO DE OENERGY
    |--------------------------------------------------------------------------
    |
    | Las siguientes líneas de idioma se utilizan para mostrar el texto del 
    | sitio web oEnergy. Usted es libre de modificar estas líneas de idioma
    | de acuerdo a los requisitos de su sitio web.
    |
    */

    /*  MENU NAVBAR */
    'inicio'                => 'home',
    'proyectos'             => 'projects',
    'blog'                  => 'blog',
    'clientes'              => 'client',
    'servicios'             => 'services',
    'quienes somos'         => 'about us',
        'empresa'               => 'company',
        'historia'              => 'history',
        'valores'               => 'values',
        'nuestra competencias'  => 'Our competences',
        'nuestro equipo'        => 'Our team',
    'español'               => 'spanish',
    'ingles'                => 'english',
    /*  FOOTER */
    'proyectos'                 =>  'projects',
    'servicios'                 =>  'Services',
    'proyectos'                 =>  'Projects',
    'contáctenos'               =>  'Contact Us',
    'política de privacidad'    =>  'Privacy Policy',
    'preguntas frecuentes'      =>  'Frequently Asked Questions',
    
    // VIEWS
    /*  VIEW:INDEX */

        /*  CAROUSEL */
        'carousel_1_titulo'      =>  'WE PROVIDE ENERGY SERVICES',
        'carousel_1_texto'       =>  'We design, build, assemble, install and maintain equipment to produce electric power.',
        'carousel_2_titulo'      =>  'OUR CHALLENGE IS',
        'carousel_2_texto'       =>  'Bring the emerging ERNC generation market to new clients and new ERNC generation projects.',
        'carousel_3_titulo'      =>  'CONSULTANCY AND TECHNICAL STUDIES',
        'carousel_3_texto'       =>  'We provide technical support to interconnect ERNC generation projects.',
            // BUTTON
            'nuestros servicios'    =>  'Our services',
            'asesorias'             =>  'Advice',

        /* SECTION:1*/
            'section_1_titulo'          =>  'TIME IS IMPORTANT AND WE KNOW IT',
            'section_1_texto'           =>  'We focus our efforts on guiding our clients towards the right decisions, we have a great team to do so.',
            
        /* SECTION:2*/
            'section_2_titulo'          =>  'ABOUT OENERGY',
            'section_2_texto'           =>  'Founded in 2013, it was born as a result of the shortage of key-in-hand project developers that can offer individual solutions and guarantee the projected savings.',

            'section_2_subtitulo_1'     =>  'SOLUTIONS',
            'section_2_texto_1'         =>  'oEnergy offers comprehensive and modular solutions that are easy to implement for different segments.',

            'section_2_subtitulo_2'     =>  'TECHNOLOGIES',
            'section_2_texto_2'         =>  'We work directly with suppliers of wind, photovoltaic, water, hydrokinetic and wave technologies.',

            'section_2_subtitulo_3'     =>  'PROJECTS',
            'section_2_texto_3'         =>  'We are a company formed by a multidisciplinary team that focuses on turnkey and Energy Savings Company (ESCO) projects.',

            'section_2_subtitulo_4'     =>  'SERVICES.',
            'section_2_texto_4'         =>  'oEnergy has the ability to deliver services at any stage of a project.',
            // BUTTON
            'saber más'     =>  'know more',
            'saber_mas'     =>  'know more',

        /* SECTION:3*/
            'section_3_titulo'      =>  'OUR PROJECTS',
            'section_3_texto'       =>  'We are a company formed by a multidisciplinary team that focuses on turn-key projects and Energy Savings Company (ESCO) types of renewable electric power with power between 2.5KW and 3MW.',
            // BUTTON
            'ver más'       =>  'SEE MORE',
            'ver_mas'       =>  'SEE MORE',

    /*  VIEW:VALUES */
    'texto_valores_1'    =>  'Our values are the foundation of our company. They define who we are and how we work, they never change. They guide our relationship with our customers, suppliers, shareholders and communities.',
    'nuestros valores'   =>  'our values',
    'compromiso'         =>  'commitment',
    'texto_valores_2'    =>  'Commitment to offer excellence in terms of innovation, individualized solution, technical quality, service, security and competitive costs.',
    'liderazgo'          =>  'leadership',
    'texto_valores_3'    =>  'Sustain our flexibility and adaptability as we continue to forge new frontiers.',
    'integridad'         =>  'integrity',
    'texto_valores_4'    =>  'Keep our transparency and ethics at all times and communicate clearly with our customers.',
    'satisfacción'       =>  'satisfaction',
    'texto_valores_5'    =>  'We are pleased to protect the environment and at the same time deliver energy efficiency and profitability to our customers.',

    /*  VIEW:CONTACT */
    'texto_contacto_1'    =>  'Contact us',
    'texto_contacto_2'    =>  'Call us or send the online form to request a quote or ask general questions about our services. We hope to help you!',
    'direccion'           =>  'address',
    'direcciones'         =>  'addresses',
        /* FORMULARIO */
        'nombre'        =>  'first name',
        'apellido'      =>  'last name',
        'correo'        =>  'email',
        'mensaje'       =>  'message',
        'enviar'        =>  'Send Message',

    /*  VIEW:SERVICES */
        'estudios_sistemas_electricos'                  => 'Estudios de Sistemas Eléctricos',
            'estudios_sistemas_electricos_description'      => 'Enfocados a determinar condiciones o limitaciones de las redes candidatas a conectarse...',
        'operacion_mantención'                          => 'Operación y Mantención',
            'operacion_mantención_description'              => 'Contratos individualizados de plantas de generación eléctrica renovable...',
        'factibilidad'                                  => 'Factibilidad',
            'factibilidad_description'                      => 'Técnico-Económico-Legal de Proyectos de Generación...',
        'due_diligence'                                 => 'Due diligence', 
            'due_diligence_description'                     => 'Due Diligence Técnico y legal para asesoramiento de compra-venta de proyectos de generación...', 
        'servicios_terreno'                             => 'Servicios en Terreno',
            'servicios_terreno_description'                 => 'oEnergy realiza estudios eléctricos que permiten determinar los efectos que producirá la operación...',
        'financiamiento_subsidios'                      => 'Financiamiento y Subsidios',
            'financiamiento_subsidios_description'          => 'Para proyectos sobre 500 kW, oEnergy financiará proyectos por medio de una ESCO...',
        'construccion'                                  => 'Construcción',
            'construccion_description'                      => 'Estudios de mercado, “power purchase agreement”, gestión de concesiones y permisos...',
        'diseño_desarrollo'                             => 'Diseño y Desarrollo',
            'diseño_desarrollo_description'                 => 'Desarrollo ingeniería de concepto, básica y de detalle de proyectos de generación renovable...',
            
    /* VIEWS ERRORS */
    /* 404 */
    '404'           =>  '404 - NOT FOUND',
    '404_text_1'    =>  'Unfortunately, the page you tried accessing could not be retrieved.',
];
