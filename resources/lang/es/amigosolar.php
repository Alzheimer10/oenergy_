<?php

return [

    /*
    |--------------------------------------------------------------------------
    | AmigoSolar!!!
    |--------------------------------------------------------------------------
    |
    */

    'direccion' => 'Av. Nueva Providencia #1881, Oficina #318 Providencia, Región Metropolitana, Chile',
    'verPDF' => 'Ver PDF',

    //NAVBAR
    'monitoreoElectrico'  =>  'Monitoreo eléctrico',
    'suministroElectrico'  =>  'Suministro eléctrico',
    'servicioGestionElectrica'  =>  'Servicio de Gestión Eléctrica®',

    //AMIGO SOLAR
    'amigoSolar' => 'Amigo Solar',
    'quienesSomos' => 'Quiénes somos',
    'valoresProposito' => 'Valores y propósito',
    'directoresEjecutivos' => 'Directores y ejecutivos',
    'practicaGC' => 'Gobierno corporativo',
    'inversionistas' => 'Inversionistas',
    'trabajaConNosotros' => 'Trabaja con nosotros',

    //Informacion Comercial
    'informacionComercial' =>  'Información Comercial',
    'ahorroGarantizado' =>     'Ahorro garantizado',
    'planesTarifas'  =>  'Planes de monitoreo',
    'servicios'  =>  'Servicios',
    'planesSuministro'  =>  'Planes de suministro',
    'servicioElectrica' =>   'Servicio de gestión eléctrica®',
    'conoceBoleta'  =>  'Conoce tu boleta',
    'medicionInteligente' =>   'Medición inteligente',
    'portalMiEnergia' =>   'Portal MiEnergía',
    'preguntasFrecuentes' =>   'Preguntas frecuentes',
        //BOLETA
        'b01'  =>  'Número de consumidor',
        'b02'  =>  'Información del plan contratado',
        'b03'  =>  'Dirección de envío de la boleta',
        'b04'  =>  'Fecha de emisión de la boleta',
        'b05'  =>  'Fecha de corte del suministro',
        'b06'  =>  'Total a pagar',
        'b07'  =>  'Período de facturación',
        'b08'  =>  'Lugares de pago',
        'b09'  =>  'Contacto de soporte técnico y comercial',
        'b10'  =>  'Reclamos y sugerencias',
        'b11'  =>  'Verificar ligitimidad de la boleta',
        'b12'  =>  'Detalle de mi cuenta',
        'b13'  =>  'Periodo de lectura',
        'b14'  =>  'Multitarifa (tramos horarios)',
        'b15'  =>  'Electricidad consumida',
        'b16'  =>  'Número de medidor',
        'b17'  =>  'Límite de invierno',
        'b18'  =>  'Gráfico 1 (consumo mes actual)',
        'b19'  =>  'Gráfico 2 (consumo últimos 13 meses)',
        'b20'  =>  'Consejos para ahorrar',
        'b21'  =>  'Mi ahorro mensual',
        
    //INFORMACION LEGAL
    'informacionLegal' => 'Información Legal',
    'terminosUso' => '{0}Términos de uso|{1}términos y condiciones de uso',
    'politicaPrivacidad' => '{0}Política de privacidad |{1}Política de privacidad y seguridad de datos|{2}Política de privacidad de datos',
    'contratoSuministro' => 'Contrato de monitoreo',
    'contratoSGE' => '{0}Contrato de suministro|{1}Contrato de suministro',
    'marcoRegulatorio' => 'Marco regulatorio',

    //COMUNIDAD
    'comunidad' => 'Comunidad',
    'noticias' => 'Noticias',
    'embajadorsolar' => 'Embajador solar',
    'proveedores' => 'Proveedores',
    'contacto' => 'Contacto',
    'enlacesInteres' => 'Enlaces de interés',
    'blog' => 'Blog',
    'SGE' => 'SGE®',
    'R' => '®',
];
