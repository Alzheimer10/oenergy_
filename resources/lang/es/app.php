<?php

return [

    /*
    |--------------------------------------------------------------------------
    | LINEAS DE TEXTO DE OENERGY
    |--------------------------------------------------------------------------
    |
    | Las siguientes líneas de idioma se utilizan para mostrar el texto del 
    | sitio web oEnergy. Usted es libre de modificar estas líneas de idioma
    | de acuerdo a los requisitos de su sitio web.
    |
    */

    /*  MENU NAVBAR */
    'inicio'             => 'inicio',
    'proyectos'             => 'proyectos',
    'blog'                  => 'blog',
    'clientes'              => 'clientes',
    'servicios'             => 'servicios',
    'quienes somos'         => 'quienes somos',
        'empresa'               => 'empresa',
        'historia'              => 'historia',
        'valores'               => 'valores',
        'nuestra competencias'  => 'Nuestra competencias',
        'nuestro equipo'        => 'Nuestro equipo',
    'español'               => 'spanish',
    'ingles'                => 'english',
    /*  FOOTER */
    'proyectos'                 =>  'proyectos',
    'servicios'                 =>  'Servicios',
    'proyectos'                 =>  'Proyectos',
    'contáctenos'               =>  'Contáctenos',
    'política de privacidad'    =>  'Política de privacidad',
    'preguntas frecuentes'      =>  'Preguntas Frecuentes',

    // VIEWS
    /*  VIEW:INDEX */

        /*  CAROUSEL */
        'carousel_1_titulo'      =>  'PROVEEMOS SERVICIOS ENERGÉTICOS',
        'carousel_1_texto'       =>  'Diseñamos, construimos, montamos, instalamos y mantenemos equipamiento para producir energía eléctrica.',
        'carousel_2_titulo'      =>  'NUESTRO DESAFÍO ES',
        'carousel_2_texto'       =>  'Acercar el emergente mercado de generación de ERNC a nuevos clientes e nuevos proyectos de generación ERNC.',
        'carousel_3_titulo'      =>  'ASESORÍAS Y ESTUDIOS TÉCNICOS',
        'carousel_3_texto'       =>  'Entregamos soporte técnico para interconectar proyectos de generación ERNC.',
            // BUTTON
            'nuestros servicios'    =>  'Nuestros servicios',
            'asesorias'             =>  'Asesorías',

        /* SECTION:1*/
            'section_1_titulo'          =>  'EL TIEMPO ES IMPORTANTE Y LO SABEMOS',
            'section_1_texto'           =>  'Centramos los esfuerzos en orientar a nuestros clientes hacia decisiones acertadas, tenemos un gran equipo para ello.',

        /* SECTION:2*/
            'section_2_titulo'          =>  'ACERCA DE OENERGY',
            'section_2_texto'           =>  'Fundada en 2013, nace producto de la escasez de empresas desarrolladoras de proyectos llave-en-mano que puedan ofrecer soluciones individualizadas y que garanticen los ahorros proyectados.',

            'section_2_subtitulo_1'     =>  'SOLUCIONES',
            'section_2_texto_1'         =>  'oEnergy ofrece soluciones integrales y modulares fáciles de implementar para distintos segmentos.',

            'section_2_subtitulo_2'     =>  'TECNOLOGÍASS',
            'section_2_texto_2'         =>  'Trabajamos directamente con los proveedores de tecnologías eólicas, fotovoltaicas, hídricas, hidrocinéticas y undimotriz.',

            'section_2_subtitulo_3'     =>  'PROYECTOS',
            'section_2_texto_3'         =>  'Somos una empresa formada por un equipo multidisciplinario que se enfoca en proyectos llave-en-mano y de tipo Energy Savings Company (ESCO).',

            'section_2_subtitulo_4'     =>  'SERVICIOS',
            'section_2_texto_4'         =>  'oEnergy tiene la capacidad de entregar servicios en cualquier etapa de un proyecto.',
            // BUTTON
            'saber más'     =>  'saber más',
            'saber_mas'     =>  'saber más',

        /* SECTION:3*/
            'section_3_titulo'      =>  'NUESTROS PROYECTOS',
            'section_3_texto'       =>  'Somos una empresa formada por un equipo multidisciplinario que se enfoca en proyectos llave-en-mano y de tipo Energy Savings Company (ESCO) de generación eléctrica renovable con potencias entre 2,5KW y 3MW.',
            // BUTTON
            'ver más'       =>  'ver más',

    /*  VIEW:VALUES */
        'texto_valores_1'    =>  'Nuestros valores son la fundación de nuestra empresa. Definen quienes somos y como trabajamos , nunca cambian. Ellos guían nuestra relación con nuestros clientes, proveedores, accionistas y comunidades.',
        'nuestros valores'   =>  'nuestros valores',
        'compromiso'         =>  'compromiso',
        'texto_valores_2'    =>  'Compromiso a ofrecer excelencia en términos de innovación, solución individualizada, calidad técnica, servicio, seguridad y costos competitivos.',
        'liderazgo'          =>  'liderazgo',
        'texto_valores_3'    =>  'Sustentar nuestra flexibilidad y adaptabilidad a medida que continuamos forjando nuevas fronteras.',
        'integridad'         =>  'integridad',
        'texto_valores_4'    =>  'Mantener nuestra transparencia y ética a toda hora y comunicarnos claramente con nuestros clientes.',
        'satisfacción'       =>  'satisfacción',
        'texto_valores_5'    =>  'Nos satisface proteger el medio ambiente y a su vez entregar eficiencia energética y rentabilidad a nuestros clientes.',
    
    /*  VIEW:CONTACT */
        'texto_contacto_1'    =>  'Póngase en contacto con nosotros',
        'texto_contacto_2'    =>  'Llámenos o envie el formulario en línea para solicitar un presupuesto o hacer preguntas generales sobre nuestros servicios. Esperamos poder ayudarle!',
        'direccion'           =>  'direccion',
        'direcciones'         =>  'direcciones',
            /* FORMULARIO */
            'nombre'        =>  'nombre',
            'apellido'      =>  'apellido',
            'correo'        =>  'correo',
            'mensaje'       =>  'mensaje',
            'enviar'        =>  'enviar',

    /*  VIEW:SERVICES */
        'estudios_sistemas_electricos'                  => 'Estudios de Sistemas Eléctricos',
            'estudios_sistemas_electricos_description'      => 'Enfocados a determinar condiciones o limitaciones de las redes candidatas a conectarse...',
        'operacion_mantención'                          => 'Operación y Mantención',
            'operacion_mantención_description'              => 'Contratos individualizados de plantas de generación eléctrica renovable...',
        'factibilidad'                                  => 'Factibilidad',
            'factibilidad_description'                      => 'Técnico-Económico-Legal de Proyectos de Generación...',
        'due_diligence'                                 => 'Due diligence', 
            'due_diligence_description'                     => 'Due Diligence Técnico y legal para asesoramiento de compra-venta de proyectos de generación...', 
        'servicios_terreno'                             => 'Servicios en Terreno',
            'servicios_terreno_description'                 => 'oEnergy realiza estudios eléctricos que permiten determinar los efectos que producirá la operación...',
        'financiamiento_subsidios'                      => 'Financiamiento y Subsidios',
            'financiamiento_subsidios_description'          => 'Para proyectos sobre 500 kW, oEnergy financiará proyectos por medio de una ESCO...',
        'construccion'                                  => 'Construcción',
            'construccion_description'                      => 'Estudios de mercado, “power purchase agreement”, gestión de concesiones y permisos...',
        'diseño_desarrollo'                             => 'Diseño y Desarrollo',
            'diseño_desarrollo_description'                 => 'Desarrollo ingeniería de concepto, básica y de detalle de proyectos de generación renovable...',
    /* VIEWS ERRORS */
    /* 404 */
    '404'           =>  '404 - PAGINA NO ENCONTADA',
    '404_text_1'    =>  'Lamentablemente, no se pudo recuperar la página que intentó acceder.',

];
