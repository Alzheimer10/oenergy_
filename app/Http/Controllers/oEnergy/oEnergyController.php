<?php

namespace App\Http\Controllers\oEnergy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class oEnergyController extends Controller
{
    public function home(){
    	return view('oEnergy.home');
    }

    public function projects(){
    	return view('oEnergy.page.projects');
    }

    public function services(){
    	return view('oEnergy.page.services');
    }

    public function service($service){
        
        $services = array(
            'estudios_sistemas_electricos',
            'operacion_mantención',
            'factibilidad',
            'due_diligence',
            'servicios_terreno',
            'financiamiento_subsidios',
            'construccion',
            'diseño_desarrollo'
        );

        if (in_array($service,$services)) {
            return view('oEnergy.page.services.service')->with('service',$service);
        }
        return view('errors.404');
    }

    public function about(){
    	return view('oEnergy.page.about');
    }

    public function contact(){
    	return view('oEnergy.page.contact');
    }

    public function blog(){
    	return view('oEnergy.page.blog');
    }

    public function fap(){
        return view('oEnergy.page.fap');
    }

    public function electrical_systems(){
        return view('oEnergy.page.electrical_systems');
    }

    public function history(){
        return view('oEnergy.page.history');
    }

    public function team($boss=null){
        if($boss==null)
            return view('oEnergy.page.team');
        else{
            if (in_array($boss,array('CSZ','YAS','JSH','FYE','LHF','RSZ','MCP')))
                return view('oEnergy.page.team')->with('boss',$boss);
            else
                abort(404);
        }
    }

    public function competition(){
        return view('oEnergy.page.competition');
    }

    public function values(){
        return view('oEnergy.page.values');
    }
}
